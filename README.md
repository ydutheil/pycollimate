# pycollimate

Module to enhance MADX tracking routine. It needs: numpy, prettytable and scipy.


The files of this folder are:

 - **madxColl** patched version of MADX. When an element RCOLLIMATOR (or
COLLIMATOR) is found, it automatically uses the pycollimate tracking
routing to track protons inside the collimator jaw
 - **track_inside_coll.py** script example on how to use pycollimate. To
   be included in the working folder.
    - It implements now a full crystal 2D pdf from measurements in H8 provided by UA9 collaboration
 - **colldbmadx_ti2.tfs** collimator database for TI2. Also to be
   included in the working directory. 
